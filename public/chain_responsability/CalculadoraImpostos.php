<?php
class CalculadoraImpostos
{


// ------------------------| Quarta abordagem

    public function calcula(Orcamento $orcamento, Imposto $imposto)
    {
        return $imposto->calcula($orcamento);
    }



    // ------------------------| Terceira abordagem
    // require "ICMS.php";
    // require "ISS.php";
    //

    // public function calcula(Orcamento $orcamento, $imposto)
    // {
    //     if ($imposto == 'ICMS') {
    //          return (new ICMS())->calcula($orcamento);
    //     } elseif ($imposto == 'ISS') {
    //          return (new ISS())->calcula($orcamento);
    //     }
    // }

    // ------------------------| Segunda abordagem

    // public function calcula(Orcamento $orcamento, $imposto)
    // {
    //     if ($imposto == 'ICMS') {
    //          return $orcamento->getValor() * 0.05;
    //     } elseif ($imposto == 'ISS') {
    //          return $orcamento->getValor() * 0.1;
    //     }
    // }



    // ------------------------| Primeira abordagem

    // public function calculaICMS(Orcamento $orcamento)
    // {
    //     return $orcamento->getValor() * 0.05;
    // }

    // public function calculaISS(Orcamento $orcamento)
    // {
    //     return $orcamento->getValor() * 0.1;
    // }
}
