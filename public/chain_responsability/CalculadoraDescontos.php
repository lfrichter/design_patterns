<?php
require_once "Desconto5Items.php";
require_once "Desconto500Reais.php";
require_once "Desconto300Reais.php";
require_once "SemDesconto.php";
class CalculadoraDescontos
{

    // ------------------------| Terceira abordagem
    public function desconto(Orcamento $orcamento)
    {
        $desconto5Items   = new Desconto5Items();
        $desconto500Reais = new Desconto500Reais();
        $desconto300Reais = new Desconto300Reais();
        $semDesconto      = new SemDesconto();

        $desconto5Items->setProximo($desconto300Reais);
        $desconto300Reais->setProximo($desconto500Reais);
        $desconto500Reais->setProximo($semDesconto);
       
        return $desconto5Items->desconto($orcamento);
    }

    // ------------------------| Segunda abordagem
    // public function desconto(Orcamento $orcamento)
    // {
    //     $desconto = new Desconto5Items();
    //     $valor_desconto = $desconto->desconto($orcamento);
    //     if ($valor_desconto == 0) {
    //         $desconto = new Desconto500Reais();
    //         $valor_desconto = $desconto->desconto($orcamento);
    //     }

    //     return $valor_desconto;
    // }

    // ------------------------| Primeira abordagem
    // public function desconto(Orcamento $orcamento)
    // {
    //     if (count($orcamento->getItems()) >= 5) {
    //         return $orcamento->getValor() * 0.1;
    //     } elseif ($orcamento->getValor() > 500) {
    //         return $orcamento->getValor() * 0.05;
    //     } else {
    //         return 0;
    //     }
    // }
}
