<?php
class Desconto5Items implements Desconto
{
    private $proximoDesconto;
    // ------------------------| Segunda abordagem
    public function desconto(Orcamento $orcamento)
    {
        if (count($orcamento->getItems()) >= 5) {
            return $orcamento->getValor() * 0.1;
        } else {
            return $this->proximoDesconto->desconto($orcamento);
        }
    }


    // ------------------------| Terceira abordagem
    public function setProximo(Desconto $proximo)
    {
        $this->proximoDesconto = $proximo;
    }
}
