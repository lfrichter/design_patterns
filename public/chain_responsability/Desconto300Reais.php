<?php
class Desconto300Reais implements Desconto
{
    private $proximoDesconto;
    // ------------------------| Segunda abordagem
    public function desconto(Orcamento $orcamento)
    {
        if ($orcamento->getValor() > 300) {
            return $orcamento->getValor() * 0.1;
        } else {
            return $this->proximoDesconto->desconto($orcamento);
        }
    }

    // ------------------------| Terceira abordagem
    public function setProximo(Desconto $proximo)
    {
        $this->proximoDesconto = $proximo;
    }
}
