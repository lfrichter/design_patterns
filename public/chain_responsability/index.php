<?php
//http://localhost/strategy/

    require "Orcamento.php";

    require "Desconto.php";
    require "Desconto500Reais.php";
    require "Desconto300Reais.php";
    require "Desconto5Items.php";
    require "SemDesconto.php";

    require "Item.php";
    require "CalculadoraImpostos.php";
    require "CalculadoraDescontos.php";
    require "Imposto.php";
    require "ICMS.php";
    require "ISS.php";
    require "KCV.php";



    $reforma = new Orcamento(301);

    $caculadora = new CalculadoraImpostos();


    echo $caculadora->calcula($reforma, new ICMS()) . '<br/>';

    echo $caculadora->calcula($reforma, new ISS()) . '<br/>';

    echo $caculadora->calcula($reforma, new KCV()) . '<br/>';

    echo "<h1>Testes de Descontos</h1>" . '<br/>';

    $calculadoraDescontos = new CalculadoraDescontos();

    echo "Desconto: ";

    $reforma->addItem(new Item('Tijolo', 250));
    $reforma->addItem(new Item('Cimento 1Kg', 250));
    $reforma->addItem(new Item('Cimento 1Kg', 250));
    $reforma->addItem(new Item('Cimento 1Kg', 250));

    echo $calculadoraDescontos->desconto($reforma) . '<br/>';
