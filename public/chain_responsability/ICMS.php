<?php

class ICMS implements Imposto
{
    // ------------------------| Terceira abordagem
    public function calcula(Orcamento $orcamento)
    {
        return $orcamento->getValor() * 0.05;
    }
}
