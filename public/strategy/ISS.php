<?php

class ISS implements Imposto
{
    // ------------------------| Terceira abordagem
    public function calcula(Orcamento $orcamento)
    {
        return $orcamento->getValor() * 0.1;
    }
}
